import express from "express";

export const router = express.Router();

export default { router };

router.get("/", (req, res) => {
  const params = {
    titulo: "EXAMEN CORTE 2",
    numDocente: req.query.numDocente,
    nombre: req.query.nombre,
    domicilio: req.query.domicilio,
    nivel: req.query.nivel,
    pagoPorHoraBase: req.query.pagoPorHoraBase,
    horasImpartidas: req.query.horasImpartidas,
    numHijos: req.query.numHijos,
    isPost: false,
  };

  res.render("index", params);
});

router.post("/", (req, res) => {
  const { nivel, pagoPorHoraBase, horasImpartidas, numHijos } = req.body;
  
  if(horasImpartidas <= 0 || pagoPorHoraBase <= 0 || numHijos < 0 || nivel < 1 || nivel > 3){
    res.redirect('/');
  }else{
      const pagoPorHorasImpartidas = calcularPago(
        pagoPorHoraBase,
        nivel,
        horasImpartidas
      ).toFixed(2);
      const impuesto = calcularImpuesto(pagoPorHorasImpartidas).toFixed(2);
      const bono = calcularBono(numHijos, pagoPorHorasImpartidas).toFixed(2);
    
      const totalAPagar = calcularTotalAPagar(pagoPorHorasImpartidas, impuesto, bono);
      const params = {
        titulo: "EXAMEN CORTE 2",
        pagoPorHorasImpartidas,
        impuesto,
        bono,
        totalAPagar,
        isPost: true,
      };
    
      res.render("index", params);
  }
});

function calcularPago(pagoPorHoraBase, nivel, horasImpartidas) {
  let pagoConBono = 0;
  pagoPorHoraBase = pagoPorHoraBase * 1;
  nivel = nivel * 1;
  horasImpartidas = horasImpartidas * 1;
  if (nivel == 1) {
    pagoConBono = pagoPorHoraBase + pagoPorHoraBase * 0.3;
  } else if (nivel == 2) {
    pagoConBono = pagoPorHoraBase + pagoPorHoraBase * 0.5;
  } else if (nivel == 3) {
    pagoConBono = pagoPorHoraBase * 2;
  }
  return pagoConBono * horasImpartidas;
}

function calcularImpuesto(pagoTotal) {
  pagoTotal = pagoTotal * 1;
  return pagoTotal * 0.16;
}

function calcularBono(numHijos, pago) {
  numHijos = numHijos * 1;
  pago = pago * 1;
  if (numHijos == 0) {
    return pago * 0.05;
  } else if (numHijos == 1) {
    return pago * 0.1;
  } else if (numHijos == 2) {
    return pago * 0.2;
  } else {
    return 0;
  }
}

function calcularTotalAPagar(pagoPorHorasImpartidas, impuesto, bono){
    pagoPorHorasImpartidas = pagoPorHorasImpartidas * 1;
    impuesto = impuesto * 1;
    bono = bono * 1;
    return (pagoPorHorasImpartidas - impuesto + bono).toFixed(2);
}